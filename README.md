# BoE /bō/


A chat platform that intends to look at other chat programs and take only the "Best of Everything".

[<img src="https://gitlab.com/ParkerMc/BoE/raw/master/B.o.E..png" alt="Icon" data-canonical-src="https://gitlab.com/ParkerMc/BoE/raw/master/B.o.E..png" width="200" height="200" />](https://gitlab.com/ParkerMc/BOE)

<sup><sup>Icon by angelgal246.</sup></sup>

### Goals:

### Development:
Anyone is welcome to contribute to the project.

Any commit must pass the following:
* [golint](https://github.com/golang/lint)
* [go vet](https://golang.org/cmd/vet/)
* [gofmt](https://golang.org/cmd/gofmt)
* [go test](https://golang.org/cmd/go/#hdr-Test_packages)

#### Setup:
Clone the repository into the `$GOPATH/src/gitlab.com/ParkerMc/BoE`. then run `make setup`, which will automatically run `go get` for you to grab all of the dependencies.

#### Building:
To build to everything run `make build`.

#### Running:
To run the server in the development environment run `make run`.

#### Docker Database:
The make file includes a docker mongo database. If you already have a database hosted on your computer, you can use that instead.
To start the database run `start-database`.  
To stop it run `stop-database`.
