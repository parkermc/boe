package main

import (
	"flag"
	"log"
	"os"
	"os/signal"

	"gitlab.com/parkermc/boe/boe"
	"gitlab.com/parkermc/boe/webserver"
)

var version = "dev"
var configFile = "./config.json"

func main() {
	// Handle flags
	flag.StringVar(&configFile, "config", configFile, "set the the config file to use")
	flag.Parse()

	core := boe.New(configFile, version, new(webserver.Webserver)) // Create and start the core
	core.Start()

	stopSignal := make(chan os.Signal, 1) // Create the stop signal handler
	signal.Notify(stopSignal, os.Interrupt)

	for range stopSignal { // Wait for the stop signal
		// Stop everything
		log.Print("Got interrupt signal")
		core.Stop()

		os.Exit(0)
	}
}
