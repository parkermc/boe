package boe

import (
	"os"
	"testing"

	"gitlab.com/parkermc/boe/model"
)

const (
	testPath = "./configs/test.json"
	version  = "test-version-1.0.23"
)

type testPlugin struct {
	enabled bool
}

func (p *testPlugin) GetInfo() *model.PluginInfo {
	return &model.PluginInfo{
		ID:      "test",
		Version: "test",
	}
}

func (p *testPlugin) Enable(boe model.Boe) {
	p.enabled = true
}

func (p *testPlugin) Disable(boe model.Boe) {
	p.enabled = false
}

func getTestStruct() *Boe {
	return New(testPath, version, new(testPlugin))
}

func testCleanup() {
	os.RemoveAll("./configs")
}

func TestNew(t *testing.T) {
	b := getTestStruct()
	if b.ConfigFilePath != testPath {
		t.Error("Error creating new core object, config path doesn't pass through")
	}
	if b.GetVersion() != version {
		t.Error("Error creating new core object, version doesn't pass through")
	}
	if b.internalPlugins["test"] == nil {
		t.Error("Plugin not passing through")
	}
	testCleanup()
}
