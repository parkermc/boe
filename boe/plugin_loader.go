package boe

import (
	"log"

	"gitlab.com/parkermc/boe/model"
)

// PluginLoader the data needed for the plugin loader
type PluginLoader struct {
	internalPlugins map[model.PluginID]model.Plugin
	plugins         map[model.PluginID]model.Plugin
}

func (b *Boe) enableEvent() {
	// Load other plugins
	for id, plugin := range b.internalPlugins { // Load internalPlugins
		plugin.Enable(b)
		b.plugins[id] = plugin
		delete(b.internalPlugins, id)
	}
	log.Print("All plugins have been started")
}

func (b *Boe) disableEvent() {
	for _, plugin := range b.plugins { // Disable plugins
		plugin.Disable(b)
	}
	log.Print("All plugins have been stopped")
}

// GetPluginConfig gets the config for a plugin
func (b *Boe) GetPluginConfig(id model.PluginID) map[string]string {
	if val, ok := b.ConfigPlugins[id]; ok {
		return val
	}
	return make(map[string]string)
}

// SetPluginConfig sets the config for a plugin
func (b *Boe) SetPluginConfig(id model.PluginID, data map[string]string) {
	b.ConfigPlugins[id] = data
}
