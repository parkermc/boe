package boe

import (
	"gitlab.com/parkermc/boe/model"
	"gitlab.com/parkermc/go-utils/configreader"
)

// Config the base for the config
type Config struct {
	ConfigFilePath string `json:"-"`

	ConfigEnabledPlugins []model.PluginID                     `json:"enabled_plugins"`
	ConfigPlugins        map[model.PluginID]map[string]string `json:"plugins"`
	ConfigVersion        string                               `json:"version"`
}

// resetConfig resets the config to it's default settings
func (b *Boe) resetConfig() {
	b.ConfigEnabledPlugins = make([]model.PluginID, 0)
	b.ConfigPlugins = make(map[model.PluginID]map[string]string)
	b.ConfigVersion = b.version
}

// LoadConfig load the config from the file
func (b *Boe) LoadConfig() error {
	b.resetConfig()
	_, err := configreader.LoadConfig(&b.Config, b.ConfigFilePath)
	if err != nil {
		return err
	}
	if b.ConfigVersion != b.version {
		// Do config update code as needed here
		b.ConfigVersion = b.version
	}
	return b.SaveConfig()
}

// SaveConfig save the config to the file
func (b *Boe) SaveConfig() error {
	return configreader.SaveConfig(&b.Config, b.ConfigFilePath)
}
