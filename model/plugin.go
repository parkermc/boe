package model

// PluginID used to help store pluginIDs
type PluginID string

// Plugin the model that all plugins must follow
type Plugin interface {
	GetInfo() *PluginInfo
	Enable(b Boe)
	Disable(b Boe)
}

// PluginInfo stores the info on the plugin
type PluginInfo struct {
	ID      PluginID
	Version string
}
