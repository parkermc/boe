.PHONY: help run build clean setup start-database test

null :=
space := $(null) #

GOPATH=$(shell go env GOPATH)
PACKAGES=$(shell go list ./...)
PACKAGES_FULL_PATH=$(GOPATH)/src/$(subst $(space), $(GOPATH)/src/,$(PACKAGES))

ifndef version
	VERSION="dev"
else
	VERSION=$(version)
endif

help: ## Show this help.
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

start-database: ## Start the database
	@echo Starting the database container

	mkdir -p run/mongo
	@if [ $(shell docker ps -a | grep -ci boe-mongo) -eq 0 ]; then \
		docker run --name boe-mongo -p 27017:27017 -d -v `pwd`/run/mongo:/data/db mongo --storageEngine wiredTiger > /dev/null; \
	elif [ $(shell docker ps | grep -ci boe-mongo) -eq 0 ]; then \
		docker start boe-mongo > /dev/null; \
	fi

stop-database: ## Stop the database
	@echo Stoping the database container

	@if [ $(shell docker ps -a | grep -ci boe-mongo) -eq 1 ]; then \
		docker stop boe-mongo > /dev/null; \
	fi

run: ## Run BoE
run: build
	@echo Running BoE

	@mkdir -p run
	@rm -rf run/bin
	@cp -r build/* run/

	cd run/; ENV=DEV ./bin/boe


build: ## Build BoE
build: setup
	@echo Building BoE

	@rm -rf build
	@mkdir -p build/bin/
	go build -ldflags "-X main.version=$(VERSION)" -o build/bin/boe main.go

setup: ## Setup the project
	@echo Setting up the project

	go get ./...

clean: ## Clean up the build files and web app
clean: stop-database
	@echo Cleaning

	rm -rf build
	rm -rf run

	@if [ $(shell docker ps -a | grep -ci boe-mongo) -eq 1 ]; then \
		docker rm -v boe-mongo > /dev/null; \
	fi

test: ## Text the code with the test files
test: setup
	@echo Running tests

	@golint $(PACKAGES)
	@go vet $(PACKAGES)
# over 10 is consittered bad code
	@gocyclo -top 10 -over 10 $(PACKAGES_FULL_PATH)

	@go test $(PACKAGES)
