package webserver

import "net/http"

type info struct {
	Version string `json:"version"`
}

func (s *Webserver) initInfo() {
	s.Routes.API.HandleFunc("/info", s.infoHandler).Methods("GET")
}

func (s *Webserver) infoHandler(w http.ResponseWriter, r *http.Request) {
	writeJSON(w, &info{
		Version: s.boe.GetVersion(),
	})
}
