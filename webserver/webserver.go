package webserver

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/parkermc/boe/model"
)

const apiVersion string = "x"

var pluginInfo = model.PluginInfo{
	ID:      "webserver",
	Version: "dev",
}

// Webserver holds the info needed for the webserver
type Webserver struct {
	Config
	Routes
	Router *mux.Router
	Server *http.Server

	boe model.Boe
}

// Routes stores all the routes
type Routes struct {
	API *mux.Router
}

// GetInfo returns the plugin info
func (s *Webserver) GetInfo() *model.PluginInfo {
	return &pluginInfo
}

// Enable enables the plugin
func (s *Webserver) Enable(boe model.Boe) {
	s.boe = boe
	s.loadConfig()
	s.Router = mux.NewRouter()

	s.API = s.Router.PathPrefix("/api/v" + apiVersion).Subrouter()
	s.initInfo()

	s.Server = &http.Server{
		Addr:    s.ConfigAddr,
		Handler: s.Router,
	}
	go func() {
		if err := s.Server.ListenAndServe(); err != nil {
			log.Fatalf("Error starting webserver: %s", err.Error())
		}
	}()
	log.Print("Webserver started")
}

// Disable disables the plugin
func (s *Webserver) Disable(boe model.Boe) {
	if err := s.Server.Close(); err != nil {
		log.Printf("Error stoping webserver: %s", err.Error())
	}
	log.Print("Webserver stopped")
}
