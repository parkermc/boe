package webserver

import "log"

// Config holds the necessary config
type Config struct {
	ConfigAddr string
}

func (s *Webserver) defaultConfig() {
	s.ConfigAddr = ":8888"
}

func (s *Webserver) loadConfig() {
	s.defaultConfig()
	data := s.boe.GetPluginConfig(pluginInfo.ID)
	if val, ok := data["address"]; ok {
		s.ConfigAddr = val
	}
	s.saveConfig()
	log.Print(s.ConfigAddr)
}

func (s *Webserver) saveConfig() {
	data := make(map[string]string)
	data["address"] = s.ConfigAddr
	s.boe.SetPluginConfig(pluginInfo.ID, data)
	s.boe.SaveConfig()
}
