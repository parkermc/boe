package webserver

import (
	"encoding/json"
	"net/http"
)

func writeJSON(w http.ResponseWriter, data interface{}) {
	jsonData, err := json.Marshal(data)
	if err != nil {
		w.WriteHeader(400)
		return
	}
	w.WriteHeader(200)
	w.Write([]byte(jsonData))
}
